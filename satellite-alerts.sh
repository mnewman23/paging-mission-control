#!/bin/bash

###########################################################################################
###########################################################################################
## Script Name:      satellite-alerts.sh                                    
## Author:           Michele Newman                                                
## Email:            mnewman2369@gmail.com  
##
## Description:      Read input file of satellite battey and thermostat readings
##                   and return json formatted alerts based on the following criteria:
##	
##                      -If for the same satellite there are three battery voltage readings 
##                       that are under the red low limit within a five minute interval.
##
##                      -If for the same satellite there are three thermostat readings 
##                       that exceed the red high limit within a five minute interval.                                         
##
###########################################################################################
###########################################################################################

## Usage of script

display_usage() { 
	echo "
Usage: $0 [ -h ] [ -f FILE ] 

Read input file of satellite battey and thermostat readings and return json formatted alerts

Options:
 -h  Display this usage page
 -f  Filename of input file

"
}

if [[ $1 == "" ]]; then
    display_usage
    exit
else
   while getopts "hf:" OPTIONS; do

  		case "$OPTIONS" in
    		h) 
				display_usage
      		exit
      		;;
    		f) 
				INPUT_FILE=${OPTARG}
      		;; 
        ?)
            echo "Error: option ${OPTARG} requires an argument" 
				exit 1
            ;;
			
  		esac
	done
fi

## Setting global variables

TMP="/tmp"
OUTPUT_FILE="$TMP/alerts.json"

## Figuring out satellites and components

SATS=`awk -F"|" '{print $2}' $INPUT_FILE | sort -n | uniq`
COMPS=`awk -F"|" '{print $8}' $INPUT_FILE | sort -n | uniq`


## Seperate each line from input file into different files for easier  
## processing, and also add timestamp in seconds since epoch for 
## easier calculation later

while read LINE; do 
	for SAT in $SATS; do
		for COMP in $COMPS; do
			if [[ $(echo $LINE| awk -F"|" '{print $2}') == $SAT && $(echo $LINE| awk -F"|" '{print $8}') == $COMP ]] ; then
				if [[ $COMP == BATT && $(echo $LINE | awk -F"|" '($7 < $6) {print "0"}') == "0" ]] ; then
					TIMESTAMP=`echo $LINE| awk -F"|" '{print $1}'`
					ETIME=$(date --date="$TIMESTAMP" +"%s")
					NEWLINE="${LINE}|${ETIME}"
					echo $NEWLINE >> $TMP/${SAT}_${COMP}
				elif [[ $COMP == TSTAT && $(echo $LINE | awk -F"|" '($7 > $3) {print "0"}') == "0" ]] ; then
					TIMESTAMP=`echo $LINE| awk -F"|" '{print $1}'`
					ETIME=$(date --date="$TIMESTAMP" +"%s")
					NEWLINE="${LINE}|${ETIME}"
					echo $NEWLINE >> $TMP/${SAT}_${COMP}
				fi
			fi
		done
	done	
done < $INPUT_FILE

## Processing each file and entries to see if they meet the 
## criteria to create an alert.  If is does the alert will be
## generated 

for SAT in $SATS; do
	for COMP in $COMPS; do
		if [ -e $TMP/${SAT}_${COMP} ]; then
			
			if [[ $(wc -l $TMP/${SAT}_${COMP} | awk '{print $1}') -ge 3 ]] ; then
				COUNT=0				
				while read LINE; do
					#echo $LINE
					case `echo $LINE| awk -F"|" '{print $8}'` in
						"BATT")
							SEV="RED LOW"
							;;
						"TSTAT")
							SEV="RED HIGH"
							;;
					esac

					if [ $COUNT -eq 0 ] ; then
						FIRST_TIME=$ETIME
						TIMESTAMP=`echo $LINE| awk -F"|" '{print $1}'`
						let COUNT++
					elif [[ $COUNT -le 2 && $(expr $ETIME - $FIRST_TIME) -le 300 ]] ; then
						let COUNT++
						if [ $COUNT -eq 2 ]; then
							echo "{\"satelliteId\": \"$SAT\", \"severity\": \"$SEV\", \"component\": \"$COMP\", \"timestamp\": \"$TIMESTAMP\"}" >> $OUTPUT_FILE 
						fi
					elif [[ $COUNT -ge 3 ]] ; then
						COUNT=1
						FIRST_TIME=$ETIME
					fi
				done < "$TMP/${SAT}_${COMP}"
			fi
		fi
	done
done


## Need to account for multiple entries to format
## pretty print for json

sed -i -e '1s/{/[{/' -e 's/}/},/g'  -e '$s/,$//' -e '$a\]' $OUTPUT_FILE


## Output final alerts

python -mjson.tool $OUTPUT_FILE


## Clean up tmp files

rm -f $OUTPUT_FILE

for SAT in $SATS; do
	for COMP in $COMPS; do
		rm -f $TMP/${SAT}_${COMP}
	done
done	

exit 0
